# Gigya to Disqus Comments Migration Tool

This project is a node script to generate an XML file (based on the Wordpress eXtended RSS (WXR) format) from JSON files exported by a Gigya administrator.

In order to be sure to have the good URLs, for each article URL in the Gigya input, the script send a request and follows the potential redirection to finally get the good URL. This final URL is used in the XML output.
To bypass Distil security system on our websites, we pass an IP to the script which is used to call the URLs.

## Usage

```
#!bash
$ ./index.js -h

  Usage: index [options]

  Options:

    -h, --help           output usage information
    -V, --version        output the version number
    --streams [file]     Streams file
    --comments [file]    Comments file
    -o, --output [file]  Output file
    --host [ip]          sfPortal IP
```

## Install

```
#!bash
$ git clone git@bitbucket.org:grenobleteam/gigya2disqus-comments.git
$ cd gigya2disqus-comments
$ npm install
$ ./index.js --streams /tmp/commentsTip/5351402_tomsitpro.com_streams.json --comments /tmp/commentsTip/5351402_tomsitpro.com_comments.json --host 54.200.154.200
Parsing data
Number of comments: 4108
Number of discussions: 1491
Parsing discussions
[=======-----------------------------------------------------------------------------------------------] 110 of 1491 (7%) 68.1s
```

## Input format

The JSON files exported by a Gigya administrator should have the following template:

### Stream file
```
#!JSON
[
    {
        "streamID": "en_US_34_Reviews_6",
        "status": "enabled",
        "streamURL": "http://www.tomsitpro.com.prex/articles/mfsys25-smb-server,2-6.html",
        "streamTitle": "Intel's 24-Core, 14-Drive Modular Server Reviewed - Introduction",
        "moderationModes": {
            "text": "inherit",
            "image": "inherit",
            "video": "inherit",
            "url": "inherit",
            "other": "inherit"
        },
        "timestamp": 1308613746444,
        "firstCommentTS": 1308592642040,
        "lastCommentTS": 1308613746135,
        "totalCommentsCount": 0,
        "categoryID": "25369178"
    },
    ...,
    { ... }
]
```

In fact, we only use the streamID and streamURL informations so it could be like this:

```
#!JSON
[
    {
        "streamID": "en_US_34_Reviews_6",
        "streamURL": "http://www.tomsitpro.com.prex/articles/mfsys25-smb-server,2-6.html",
    },
    ...,
    { ... }
]
```

### Comments file


```
#!JSON
[
    {
        "ID": "372a313dccfd4711ad242c6bcec14fdd",
        "threadID": "",
        "isModerator": false,
        "commentText": "I think an article on FUSE (http://fuse.sourceforge.net/) would be interesting in the context of IT system management.  A previous manager of mine had me experiment with this kernel module on a SUSE system.  While building and implementing FUSE was fairly straightforward, it was more in the justification and potential risks that we had problems.  There were performance issues and there were security concerns.  The sell, of course, is that it's not tied to a particular OS and you can do non-privileged mounts.  File systems are an important part of IT professionals day-to-day work and I'd be interested in reading more articles like this that take us deeper into the topic, review requirements that call for specific file system implementations and where technology is taking the file system, like with FUSE/AVFS. ",
        "TotalVotes": 0,
        "sender": {
            "IP": "76.175.73.94",
            "photoURL": "https://lh5.googleusercontent.com/--ceATYmFpHU/AAAAAAAAAAI/AAAAAAAAABk/OdKZD0CDaaI/photo.jpg",
            "profileURL": "https://plus.google.com/100845279958177635754",
            "name": "Christian Bryant",
            "isSelf": false,
            "loginProvider": "Google"
        },
        "providerPostIDs": {
            "google": "tag:google.com,2010:buzz:z133jpozakqbenet522uxzx45puotnkok04"
        },
        "flagCount": 0,
        "state": 2,
        "rejectInfo": {
            "reason": 0
        },
        "highlightGroups": [],
        "edited": false,
        "timestamp": 1310191767270,
        "threadTimestamp": 1310191767270,
        "status": "published",
        "categoryID": "25369178",
        "streamID": "en_US_34_Reviews_43"
    },
    ...,
    { ... }
]
```

In fact we don't use all fields so it could be like this:


```
#!JSON
[
    {
        "ID": "372a313dccfd4711ad242c6bcec14fdd",
        "commentText": "I think an article on FUSE (http://fuse.sourceforge.net/) would be interesting in the context of IT system management.  A previous manager of mine had me experiment with this kernel module on a SUSE system.  While building and implementing FUSE was fairly straightforward, it was more in the justification and potential risks that we had problems.  There were performance issues and there were security concerns.  The sell, of course, is that it's not tied to a particular OS and you can do non-privileged mounts.  File systems are an important part of IT professionals day-to-day work and I'd be interested in reading more articles like this that take us deeper into the topic, review requirements that call for specific file system implementations and where technology is taking the file system, like with FUSE/AVFS. ",
        "sender": {
            "IP": "76.175.73.94",
            "profileURL": "https://plus.google.com/100845279958177635754",
            "name": "Christian Bryant"
        },
        "state": 2,
        "timestamp": 1310191767270,
        "streamID": "en_US_34_Reviews_43"
    },
    ...,
    { ... }
]
```

## Output

The generated XML file (comments.xml by default) looks like this:


```
#!XML
<?xml version="1.0"?>
<disqus xmlns="http://disqus.com" xmlns:dsq="http://disqus.com/disqus-internals" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://disqus.com/api/schemas/1.0/disqus.xsd http://disqus.com/api/schemas/1.0/internals.xsd">
  <channel>
    <item>
      <link>http://www.tomsitpro.com/articles/windows-linux-file_system,2-43-2.html</link>
      <title>One File System Doesn't Fit All</title>
      <dsq:thread_identifier>BOM_comments_en_US_34_Reviews_43</dsq:thread_identifier>
      <wp:comment>
        <wp:comment_id>372a313dccfd4711ad242c6bcec14fdd</wp:comment_id>
        <wp:comment_author>Christian Bryant</wp:comment_author>
        <wp:comment_author_url>https://plus.google.com/100845279958177635754</wp:comment_author_url>
        <wp:comment_author_IP>76.175.73.94</wp:comment_author_IP>
        <wp:comment_date_gmt>2011-07-09 08:09:27</wp:comment_date_gmt>
        <wp:comment_approved>1</wp:comment_approved>
        <wp:comment_content>
          <![CDATA[I think an article on FUSE (http://fuse.sourceforge.net/) would be interesting in the context of IT system management.  A previous manager of mine had me experiment with this kernel module on a SUSE system.  While building and implementing FUSE was fairly straightforward, it was more in the justification and potential risks that we had problems.  There were performance issues and there were security concerns.  The sell, of course, is that it's not tied to a particular OS and you can do non-privileged mounts.  File systems are an important part of IT professionals day-to-day work and I'd be interested in reading more articles like this that take us deeper into the topic, review requirements that call for specific file system implementations and where technology is taking the file system, like with FUSE/AVFS. ]]>
        </wp:comment_content>
      </wp:comment>
    </item>
    ...
  </channel>
</disqus>
```