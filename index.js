#!/usr/bin/env node

var program    = require('commander');
var chalk      = require('chalk');
var fs         = require('fs');
var util       = require('util');
var moment     = require('moment');
var xmlbuilder = require('xmlbuilder');
var Q          = require('q');
var url        = require('url');
var request    = require('requestretry');
var Progress   = require('progress');

program.version('0.0.1')
	.option('--streams [file]', 'Streams file')
	.option('--comments [file]', 'Comments file')
	.option('-o, --output [file]', 'Output file')
	.option('--host [ip]', 'sfPortal IP')
	.parse(process.argv);

checkInputs();

if (!program.output) {
	program.output = 'comments.xml'
}

console.log(chalk.bold.underline.green('Parsing data'));

fs.readFile(program.comments, function(err, data) {
	if (err) throw err;
	
	var commentsAsArray = JSON.parse(data);
	
	console.log(chalk.green('Number of comments: ') + chalk.bold.bgGreen(commentsAsArray.length))
	
	discussions = commentsAsArray.reduce(function(discussions, comment, index, array) {
		discussions[comment.streamID] = discussions[comment.streamID] || {};
		discussions[comment.streamID].comments = discussions[comment.streamID].comments || [];

		discussions[comment.streamID].comments.push({
			id: comment.ID,
			author: comment.sender.name,
			author_url: comment.sender.profileURL,
			author_ip: comment.sender.IP,
			created_at: moment(comment.timestamp, 'x').format('YYYY-MM-DD HH:mm:ss'),
			content: comment.commentText,
			comment_approved: comment.state === 2 ? 1 : 0
		});

		return discussions;
	}, {});

	fs.readFile(program.streams, function(err, data) {
		if (err) throw err;

		var streamsAsArray = JSON.parse(data);
		console.log(chalk.green('Number of discussions: ') + chalk.bold.bgGreen(streamsAsArray.length));
		console.log(chalk.green('Parsing discussions'));

		var bar = new Progress('[:bar] :current of :total (:percent) :etas', { total: streamsAsArray.length });
		var promises = [];
		discussions = streamsAsArray.reduce(function(discussions, stream, index, array) {
			discussions[stream.streamID] = discussions[stream.streamID] || {};
			discussions[stream.streamID].title = stream.streamTitle;

			promises.push(getRealURL(stream.streamURL).then(function(value) {
				discussions[stream.streamID].URL = value;
				bar.tick();
			}, function() {
				bar.tick();
			}));

			return discussions;
		}, discussions);

		Q.allSettled(promises).then(function() {
			console.log(chalk.green('Generating XML'));
			generateXML(discussions, program.output);
		});
	});
});

function getRealURL(articleUrl) {
	var defer = Q.defer();

	if (typeof articleUrl === 'undefined' || articleUrl === '') {
		defer.reject();
		return defer.promise;
	}

	var explodedUrl = url.parse(articleUrl);
	var originalHost = explodedUrl.host;

	request({
		url: explodedUrl.protocol + '//' + program.host + explodedUrl.path,
		headers: {
			Host: originalHost
		}
	}, function(err, response, body) {
		if (err)  {
			defer.reject();
			return defer.promise;
		}

		var explodedRealUrl = url.parse(response.request.uri.href);
		explodedRealUrl = explodedRealUrl.protocol + '//' + originalHost + explodedRealUrl.path;

		defer.resolve(explodedRealUrl);
	});

	return defer.promise;
}

function generateXML(discussions, filename) {
	var xml = xmlbuilder.create('rss').att({
		'xmlns:content': 'http://purl.org/rss/1.0/modules/content/',
		'xmlns:dsq': 'http://www.disqus.com/',
		'xmlns:dc': 'http://purl.org/dc/elements/1.1/',
		'xmlns:wp': 'http://wordpress.org/export/1.0/'
	})
	.ele('channel');

	for (discussionId in discussions) {

		if (typeof discussions[discussionId].comments !== 'undefined' && typeof discussions[discussionId].URL === 'string') {
			var comments = xml .ele('item');
			comments.ele('link', null, discussions[discussionId].URL);
			comments.ele('title', null, discussions[discussionId].title);
			comments.ele('dsq:thread_identifier', null, 'BOM_comments_' + discussionId);

			discussions[discussionId].comments.forEach(function(comment, index) {
				var commentXML = comments.ele('wp:comment');
				commentXML.ele('wp:comment_id', null, 'gigya-v1-' + comment.id);
				commentXML.ele('wp:comment_author', null, comment.author);
				commentXML.ele('wp:comment_author_url', null, comment.author_url);
				commentXML.ele('wp:comment_author_IP', null, comment.author_ip);
				commentXML.ele('wp:comment_date_gmt', null, comment.created_at);
				commentXML.ele('wp:comment_approved', null, comment.comment_approved);
				commentXML.ele('wp:comment_content').dat(comment.content);
			});
		}
	}

	fs.writeFile(filename, xml.end({pretty: true}));
}

function error(msg, withUsage) {
	console.log(chalk.bold.red(msg));

	if (withUsage) {
		program.outputHelp();
	}

	process.exit(1);
}

function checkInputs() {
	if (!program.streams) {
		error('  Missing streams file', true);
	}
	if (!program.comments) {
		error('  Missing comments file', true);
	}
	if (!program.host) {
		error('  Missing host IP', true);
	}

	if (!fs.exists(program.streams, function(exists) {
		if (!exists) {
			error(util.format('File %s  doesn\'t exist!', program.streams));
		}
	}));

	if (!fs.exists(program.comments, function(exists) {
		if (!exists) {
			error(util.format('File %s  doesn\'t exist!', program.streams));
		}
	}));
}
